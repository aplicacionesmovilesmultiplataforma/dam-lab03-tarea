//Importacion de React y el Hook useState
import React, { useState } from 'react';
//Importacion de los componentes de react-native
import { View, StyleSheet, Button } from 'react-native';

//Importacion de los componentes
import AgeValidator from './components/ageValidator';
import MyList from './components/myList';

//Declaracion del componente como funcion usando ES6
const App = () => {
  //Uso del Hook UseState para que, según el estado, muestre un componente u otro
  //Por defecto se esta pasando el valor "ageValidator"
  const [componente, setComponente] = useState("ageValidator");

  //funcion que permite cambiar de compoente segun el valor de la variable componete
  const cambiarComponente = () => {
    if (componente == "ageValidator") {
      //Se asigna el valor "myList" a la variable componente
      setComponente("myList")
    } else {
      //Se asigna el valor "ageValidator" a la variable componente
      setComponente("ageValidator")
    }
  }

  return (
    <View style={styles.container}>
      {/**Se tiene una condicion ternaria, segun el valor de la variable componente se mostrará una View con el componente que corresponde */}
      {componente == "ageValidator"
        ?
        <View>
          <Button color='#efb810' onPress={cambiarComponente} title="Cambiar a Lista" />
          <AgeValidator />
        </View>
        :
        <View>
          <Button color='#efb810' onPress={cambiarComponente} title="Cambiar a Validator" />
          <MyList />
        </View>
      }
    </View>
  );
}

//Stylesheet para el uso de estilos
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ecf0f1',
  },
});

//Exportando el compoente App para que sea renderizado en index.js
export default App;
