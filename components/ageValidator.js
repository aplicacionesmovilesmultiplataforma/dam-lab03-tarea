//Importacion de React y el Hook useState
import React, { useState } from 'react';
//Importacion de los componentes de react-native
import { StyleSheet, View, Text, TextInput } from 'react-native';


//Declaracion del componente AgeValidator como funcion usando ES6
const AgeValidator = () => {

    //Uso del Hook UseState
    //Variable edad tiene como valor inicial ""
    const [edad, setEdad] = useState("");
    //Variable mensaje tiene como valor inicial ""
    const [mensaje, setMensaje] = useState("");

    //funcion que permite cambiar el valor de la variable edad segun el valor pasado como parametro
    //tambien setea o cambia el valor de la variable mensaje segun las condiciones establecidas
    const cambiarEdad = (tEdad) => {
        setEdad(tEdad);
        if (tEdad != "") { //Si el valor proporcionado no es vacio
            if (tEdad > 0) {  //Si el valor proporcionado es valido
                if (tEdad >= 18) { //Si el valor proporcionado es mayor o igual a 18
                    setMensaje("Es mayor de edad")
                } else { //Si el valor proporcionado es menor a 18
                    setMensaje("Es menor de edad")
                }
            } else { //Si el valor proporcionado no es valido
                setMensaje("Ingrese un valor valido")
            }
        } else {  //Si el valor proporcionado es vacio
            setMensaje("")
        }
    }

    return (
        <View style={styles.container}>
            <View>
                <Text style={styles.text}>Ingrese su edad</Text>
            </View>
            {/** Se establece que el tipo de teclado sea numerico */}
            {/** El valor del input es igual al de la variable edad */}
            {/** Cuando el valor del input cambia se llama a la funcion cambiar edad */}
            <TextInput
                keyboardType="numeric" 
                style={styles.input}
                onChangeText={text => cambiarEdad(text)}
                value={edad}
                />
            {/** El valor mostrado es el resultado de la evaluacion de la funcion cambarEdad en la variable mensaje */}
            <Text style={styles.textResult}> {mensaje}</Text>
        </View>
    );

}

//Stylesheet para el uso de estilos
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        paddingHorizontal: 10,
        height: '100%',
        justifyContent: 'space-between',
    },
    text: {
        alignItems: 'center',
        textAlign: 'center',
        padding: 10,
        fontSize: 20,
        color: 'blue'
    },
    textResult: {
        alignItems: 'center',
        textAlign: 'center',
        padding: 10,
        fontSize: 20,
        color: 'red'

    },
    input: {
        height: 40,
        borderColor: '#fff',
        borderWidth: 1,
        backgroundColor: '#3CBC8D',
        borderRadius: 10,
        color: 'white',
        paddingLeft: 10,
        marginBottom: 30,
        fontSize: 20,
    },
});

//Exportando el componente AgeValidator
export default AgeValidator;